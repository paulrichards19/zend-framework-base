<?php

class Application_Model_User extends Kit_Model_Service  {
		
	const SALT = "Z35dB4S3TW1tter";
	
	public $addErrors; 
	
	public static function logout(){
		
		// clear the cookie first
		setcookie ("authentication", "", time() - 3600, '/');
		
		// clear the identity
		$auth = Zend_Auth::getInstance();
    	$auth->clearIdentity ();    			
		
	}
	
	public function login( $email, $password ){
		
		$authAdapter = new Zend_Auth_Adapter_DbTable( $this->getDbAdapter() );
		$authAdapter
		    ->setTableName('user')
			->setIdentityColumn('email')
			->setCredentialColumn('password')
			->setCredentialTreatment('MD5(?)');  
	    
		$authAdapter
		    ->setIdentity( $email )
	    	->setCredential( $password );	    	
	    	    
	    $result = Zend_Auth::getInstance()->authenticate($authAdapter);	    
		
		if ($result->isValid()) {
			
		    $storage = Zend_Auth::getInstance()->getStorage();
		    
		    //original code (just email , user_d and role written)      
		    //$storage->write($authAdapter->getResultRowObject(array('email','user_id','role')));
			
		    //olli3
		    //suggested change
		    //writing all details from user table to storage
		    
		    // get all info about this user from the login table  
		    // ommit only the password, we don't need that  
		    $userInfo = $authAdapter->getResultRowObject(null, 'password');  
			//write to storage
		    $storage->write($userInfo);  
		    		    
		    return true;
		    
		}else{
			
			return false;
			
		}		
		
	}
	
	public function updateIdentifier(){
				
		$user_id = self::getCurrentUserId();
		
		$identifier = md5( self::SALT . md5($user_id . self::SALT ) );		
		
		$timeout = time() + 60 * 60 * 24 * 30;		
		
		$key = md5(uniqid(rand(), true));
		
		setcookie('authentication', "$identifier:$key", $timeout, '/');
		
		$table = new Zend_Db_Table('user');
		
		$where = $table->getAdapter()->quoteInto('user_id = ?', $user_id);
		
		$data = array(
			'key' => $key,
			'key_timeout' => $timeout
		);
		
		$table->update( $data, $where );
		
	}
	
	public function clearIdentifier(){		
				
		$table = new Zend_Db_Table('user');		
		$where = $table->getAdapter()->quoteInto('user_id = ?', self::getCurrentUserId() );		
		$data = array(
			'key' => '',
			'key_timeout' => 0
		);		
		$table->update( $data, $where );
		
	}
	
	public function useIdentifier(){
		
		if (isset($_COOKIE['authentication'])) {
			
			list($identifier, $key) = explode(':', $_COOKIE['authentication']);
			
			$table = new Zend_Db_Table('user');
			
			$rs = $table->fetchRow( $table->select()->where( '`key` = ?',array( $key ) ) );
			
			if($rs){
				
				if( $rs->key != $key ){
					
					// unique keys do not match
					return false;
					
				}elseif( time() > $rs->key_timeout ){
					// cookie has timed out
					
					return false;
					
				}elseif ( $identifier != md5( self::SALT . md5($rs->user_id . self::SALT ) ) ){
					// user ident does not match		
					return false;
				}else{
					// log them in, its ok
					
					$this->login( $rs->email, $rs->password );					
					
					return true;
				}
				
			}
			
		}else{
			
			return false;			
			
		}
		
	}
	
	public static function isAdmin(){

		$user = self::getCurrentUserIdentity();
		
		if ( $user->role == 'admin' ){
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
		
	public static function getCurrentUserId(){
		
		if( Zend_Auth::getInstance()->hasIdentity() ){
			
			return Zend_Auth::getInstance()->getIdentity()->user_id;
			
		}else{
			
			return false;
			
		}		
		
	}
	
	public static function getCurrentUserIdentity(){
		
		if( Zend_Auth::getInstance()->hasIdentity() ){
			
			return Zend_Auth::getInstance()->getIdentity();
			
		}else{
			
			return false;
			
		}		
		
	}
	
	public static function hasIdentity(){
		
		if( Zend_Auth::getInstance()->hasIdentity() ){
			
			return true;
			
		}else{
			
			return false;
			
		}
		
	}
	
	public function add( $email, $password1, $password2, $firstname, $lastname ){
		
		/**
		 * Validate fields
		 */
		
		$errorCount = 0;
		
		$emailValidator = new Zend_Validate_EmailAddress();		
		if( !$emailValidator->isValid($email) ){
			$errorCount++;
			$this->addErrors['email'][] = "Email not valid";
		}
		
		//olli3 suggested change
		//check for exisitng email address
		$emailExistsValidator = new Zend_Validate_Db_NoRecordExists(
		    array(
		        'table' => 'user',
		        'field' => 'email'
		    )
		);
		if( !$emailExistsValidator->isValid($email) ){
			$errorCount++;
			$this->addErrors['email'][] = "Email already registered";
		}
		
		$passwordValidator = new Zend_Validate_Identical( $password1 );
		if( !$passwordValidator->isValid($password2) ){
			$errorCount++;
			$this->addErrors['password'][] = "Passwords do not match";
		}
		
		$password2Validator = new Zend_Validate_StringLength();
		$password2Validator->setMin(5);
		$password2Validator->setMax(20);
		if( !$password2Validator->isValid($password1) ){
			$errorCount++;
			$this->addErrors['password'][] = "Password too short or long";
		}
		
		$nameValidator = new Zend_Validate_StringLength();
		$nameValidator->setMin(1);
		$nameValidator->setMax(255);
		if( !$nameValidator->isValid($firstname) ){
			$errorCount++;
			$this->addErrors['firstname'][] = "Firstname too short or long";
		}
		
		if( !$nameValidator->isValid($lastname) ){
			$errorCount++;
			$this->addErrors['lastname'][] = "Lastname too short or long";
		}
		
		if( $errorCount > 0 ){
			
			return false;
			
		}else{
			
			$table = new Zend_Db_Table('user');
			$data = array(
			    'email'     		=> $email,
			    'password' 			=> md5($password1),
				'firstname' 		=> $firstname,
				'lastname' 			=> $lastname,
			    'date_created'		=> time(),
				'role'				=> 'user',
			);
			 
			$table->insert($data);
			
			return true;
		}
		
	}
	
}