<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	/**
	 * 
	 * Set up any configs
	 */
    public function _initConfigs(){

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/values.ini', APPLICATION_ENV);
        Zend_Registry::set('config', $config);
    }
    
	protected function _initcookielogin(){
		
		$this->bootstrap('db');
		
		if ( ! Application_Model_User::hasIdentity()) {
			$login = new Application_Model_User();
			$login->useIdentifier();
		}
		
		$this->bootstrap('log');
 
        // Retrieve the front controller from the bootstrap registry
        $log = $this->getResource('log');
		
        if( $user = Application_Model_User::getCurrentUserIdentity() ){
        	$log->info( "user_id " . $user->user_id . " - " . $user->firstname . " ". $user->lastname. " - " . $user->email  );        	
        }else{        	
        	$log->info( "Not logged in."  );
        }
 	
	}
}
