<?php

class UserController extends Zend_Controller_Action
{

	public function init(){
			
		$this->view->headTitle('Login/register');
		
		$this->_redirector = $this->_helper->getHelper ( 'Redirector' );
	}
	
	public function registerAction(){
			
		$email = $this->_getParam('email');
		$firstname = $this->_getParam('firstname');
		$lastname = $this->_getParam('lastname');
		$password1 = $this->_getParam('password1');
		$password2 = $this->_getParam('password2');
		
		$user = new Application_Model_User();
		
		if( $user->add($email, $password1, $password2, $firstname, $lastname) ){
			
			$this->_forward('registersuccess',null,null,array(  ));
			
		}else{

			$this->_forward('login', null, null, array( 'addErrors' => $user->addErrors, 'email' => $email ));
			
		}
		
	
	}
	
	public function registersuccessAction(){
		
		
		
	}
		
	
	public function loginAction(){
			
		//$this->_helper->layout->setLayout('loginregister');
		Zend_Layout::getMvcInstance()->assign('showheader', false);

		if( $this->_hasParam('addErrors') ){
			
			$this->view->addErrors = $this->_getParam('addErrors');
			$this->view->email = $this->_getParam('email');
			
		}
		
		
	}
	
	public function submitAction(){
		
	     // if you are already logged in you cant be here
        
    	if(isset(Zend_Auth::getInstance()->getIdentity()->user_id)){			
			$this->_redirector->gotoSimpleAndExit ( 'index', 'index' );			
		}    	
		
		$userService = new Application_Model_User();
		
		if( $userService->login( $this->_request->getParam('email'), $this->_request->getParam('password')  ) ){
			
			if( $this->_request->getParam('rememberme') ){
				// update or set the users idenitier cookie and record the login
			    $userService->updateIdentifier();	
			}else{
				$userService->clearIdentifier();
			}
		    
			if (Zend_Uri::check ( urldecode ( $this->_request->getParam ( 'ref' ) ) )) {
				$this->_redirector->gotoUrlAndExit ( urldecode ( $this->_request->getParam ( 'ref' ) ) );
			} else {
				$this->_redirector->gotoSimpleAndExit ( 'index', 'index', 'default' );
			}			
			
		}else{
			
			// Authentication failed; print the reasons why
			$this->view->errorMessage = "The emaill address or password you entered was incorrect";
			$this->_forward ( 'index',  'index' );
			
		}
		
	}
	
	public function logoutAction(){
			
		Application_Model_User::logout();
		$this->_redirector->gotoSimpleAndExit ( 'index', 'index', 'index' );
		
	}


}