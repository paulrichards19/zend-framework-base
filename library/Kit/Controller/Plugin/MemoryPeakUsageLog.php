<?php
class Kit_Controller_Plugin_MemoryPeakUsageLog
										extends Zend_Controller_Plugin_Abstract
{
	
	protected $_log = null;
	
	public function __construct(Zend_Log $log)
	{
		$this->_log = $log;
	}
	
	public function dispatchLoopShutdown()
	{
		$peakUsage = memory_get_peak_usage(true);
		
		$url = $this->getRequest()->getRequestUri();
		
		$unit = new Zend_Measure_Binary(memory_get_peak_usage(true), Zend_Measure_Binary::BYTE  );
				
		$this->_log->info('Memory peak at ' . $unit->convertTo(Zend_Measure_Binary::MEGABYTE) . ' of ' . ini_get('memory_limit') );
	}
	
}