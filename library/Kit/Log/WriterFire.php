<?php

class Kit_Log_WriterFire extends Zend_Log_Writer_Firebug {
	
	
	public static $start_time;
	
	public static $last_time;
	
	public static $mdg_time = 0;
	
	public static $mdg_requests = 0;
	
	public static function initStartTime(){
		
		self::$start_time = microtime(true);
	}
	
	public function __construct(){
		
		parent::__construct();
		
		$this->_message = new Zend_Wildfire_Plugin_FirePhp_TableMessage('Execution Timer');
		$this->_message->setBuffered(true);
		$this->_message->setHeader(array('Time','Split','Message'));
		$this->_message->setDestroy(true);
		$this->_message->setOption('includeLineNumbers', false);
		
		Zend_Wildfire_Plugin_FirePhp::getInstance()->send($this->_message);
				
		
	}	

	
    /**
     * Log a message to the Firebug Console.
     *
     * @param array $event The event data
     * @return void
     */
    final function _write($event)
    {
        if (!$this->getEnabled()) {
            return;
        }

        if (array_key_exists($event['priority'],$this->_priorityStyles)) {
            $type = $this->_priorityStyles[$event['priority']];
        } else {
            $type = $this->_defaultPriorityStyle;
        }

        $message = $this->_formatter->format($event);

        $label = isset($event['firebugLabel'])?$event['firebugLabel']:null;		
        
        if( $event['priorityName'] == 'TIMER' ){
        	
        	$time_end = microtime(true);
			$this->_message->setDestroy(false);
			$current_time = round($time_end - self::$start_time, 4);
        	$this->_message->addRow(array($current_time,(round($current_time-self::$last_time,4)),$message ));
        	self::$last_time = $current_time;        	 
      	 
        	$this->_message->setLabel("Execution Timer: $current_time secs");

        }else{
                Zend_Wildfire_Plugin_FirePhp::getInstance()->send($message,
                                                          $label,
                                                          $type,
                                                          array('traceOffset'=>6));
        }  

    }
	
}