<?php
/**
 * Base class for all Services
 *
 * This class provides a common api for accessing MDG Contexts, Database resources or cache backends
 *
 * @author Marcel Beerta <marcel.beerta@interactivedata.com>
 * @package WebToolkit
 * @subpackage Model
 * @version $Id$
 * @copyright 2009 Interactive Data Managed Solutions
 */
abstract class Kit_Model_Service
{


    /**
     * The default Database Adapter used when requesting Database Data
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected static $_defaultDbAdapter;

    /**
     * The default cache instance to use for caching
     *
     * @var Zend_Cache_Core
     */
    protected static $_defaultCache;
    
    /**
     * The default log instance to use for logging
     *
     * @var Zend_Log
     */
    protected static $_defaultLog;
    
    /**
     * 
     *
     * @var Zend_Auth
     */
    protected static $_defaultAuth;
      

    /**
     * The class instance's Database Adapter
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_dbAdapter = null;

    /**
     * The cache object to use when caching data
     *
     * @var Zend_Cache
     */
    protected $_cache = null;
    
    /**
     * The log object to use when logging data
     *
     * @var Zend_Log
     */
    protected $_log = null;

    /**
     * 
     *
     * @var Zend_Auth
     */
    protected $_auth = null;
    
    /**
     * Returns the Database adapter which provides access to the database
     * 
     * @return Zend_Db_Adapter_Abstract
     */
    public function getDbAdapter ()
    {
        return ( $this->_dbAdapter !== null ) ? $this->_dbAdapter : self::$_defaultDbAdapter;
    
    }

    /**
     * Returns the cache instance which can be used to cache non-volatile items
     * @return Zend_Cache_Core
     */
    public function getCache ()
    {
        return ( $this->_cache !== null ) ? $this->_cache : self::$_defaultCache;
    }
    
    /**
     * 
     * @return Zend_Log
     */
    public function getLog ()
    {
        return ( $this->_log !== null ) ? $this->_log : self::$_defaultLog;
    }
    
    /**
     * 
     * @return Zend_Auth
     */
    public function getAuth ()
    {
        return ( $this->_auth !== null ) ? $this->_auth : self::$_defaultAuth;
    }
    
    /**
     * Sets the MDG Context for requesting MDG Data
     *
     * @var MDG_Context $context
     */
    public function setMdgContext ( MDG_Context $context )
    {
        $this->_mdgContext = $context;
    }

    /**
     * Sets the Database Adapter for requesting Database Data
     *
     * @var Zend_Db_Adapter_Abstract $db
     */
    public function setDbAdapter ( Zend_Db_Adapter_Abstract $db )
    {
        $this->_dbAdapter = $db;
    }

    /**
     * Sets the Cache to use
     *
     * @var Zend_Cache_Core
     */
    public function setCache ( Zend_Cache_Core $cache )
    {
        $this->_cache = $cache;
    }
    
    /**
     * Sets the log to use
     *
     * @var Zend_Log
     */
    public function setLog ( Zend_Log $log )
    {
        $this->_log = $log;
    }
    
    /**
     * 
     *
     * @var Zend_Auth
     */
    public function setAuth ( Zend_Auth $auth )
    {
        $this->_auth = $auth;
    }
       

    /**
     * Sets the Default Database Adapter
     *
     * @var Zend_Db_Adapter_Abstract $db
     */
    public static function setDefaultDbAdapter ( Zend_Db_Adapter_Abstract $db )
    {
        self::$_defaultDbAdapter = $db;
    }

    /**
     * Sets the Default Cache
     *
     * @var Zend_Cache
     */
    public static function setDefaultCache ( Zend_Cache_Core $cache )
    {
        self::$_defaultCache = $cache;
    }
    
    /**
     * Sets the Default Log
     *
     * @var Zend_Log
     */
    public static function setDefaultLog ( Zend_Log $log )
    {
        self::$_defaultLog = $log;
    }
    
    /**
     * 
     *
     * @var Zend_Auth
     */
    public static function setDefaultAuth ( Zend_Auth $auth )
    {
        self::$_defaultAuth = $auth;
    }
    
    
}