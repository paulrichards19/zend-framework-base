<?php 

class Kit_Model_Paginator implements Zend_Paginator_Adapter_Interface {
	
	private $_model;
	
	public function __construct( $model ){
		
		$this->_model = $model;
		
	}
	
	/* (non-PHPdoc)
	 * @see Zend_Paginator_Adapter_Interface::getItems()
	 */
	public function getItems($offset, $itemCountPerPage) {
		
		// covert to pages rather than rows
		$OFFSET = ceil( $offset / $itemCountPerPage );
		$BLOCKSIZE = $itemCountPerPage;
		
		return $this->_model->getDataPaginator( $OFFSET, $BLOCKSIZE, $itemCountPerPage );
		
	}

	/* (non-PHPdoc)
	 * @see Countable::count()
	 */
	public function count() {
		// TODO Auto-generated method stub
		
		return $this->_model->getCountPaginator();
		
	}	
	
	
}