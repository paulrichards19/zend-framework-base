<?php 

interface Kit_Model_PaginatorInterface {
	
	public function getDataPaginator( $OFFSET, $BLOCKSIZE, $itemCountPerPage );
	
	public function getCountPaginator();
	
}