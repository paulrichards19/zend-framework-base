<?php

class Kit_Application_Resource_Log extends Zend_Application_Resource_Log
{
		
    public function init ()
    {
            
		$params = $this->getOptions();
		
		$logger = new Zend_Log (  );		
		$logger->addPriority('timer', 8);
		
		if (  $params['timer']['logtofirephp'] )
		{
			
			$writer = new Kit_Log_WriterFire ( );
			$writer->setPriorityStyle(8, 'TABLE');
			$writer->setPriorityStyle(9, 'TABLE');
			
			// add the memory usage
			$plugin = new Kit_Controller_Plugin_MemoryPeakUsageLog($logger);
	        $this->getBootstrap()->bootstrap('frontController');
	        /* @var $frontController Zend_Controller_Front */
	        $frontController = $this->getBootstrap()->getResource('frontController');
	        $frontController->registerPlugin($plugin);			
			
		}else{
			
			$writer = new Zend_Log_Writer_Null ( );
			
		}
		
		if (  $params['db']['logtofirephp'] )
		{
			
	    	$this->getBootstrap()->bootstrap( 'db' );
	    	$db = $this->getBootstrap()->getResource('db');
	    	
			$profiler = new Zend_Db_Profiler_Firebug ( 'All DB Queries' );
			$profiler->setEnabled ( true );
			$db->setProfiler ( $profiler );   
			
		}
		
		$logger->addWriter($writer);
		
        return $logger;
    }

    
}
