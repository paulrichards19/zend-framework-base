<?php

class Kit_Application_Resource_Service extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * Initializes the Abstract Service class with the default resources
     *
     * @return void
     */
    public function init()
    {
        $this->getBootstrap()->bootstrap('db');
        $this->getBootstrap()->bootstrap('log');
        $this->getBootstrap()->bootstrap('cachemanager');

        Zend_Registry::set('db', $this->getBootstrap()->getResource('db'));
		Zend_Registry::set('log', $this->getBootstrap()->getResource('log'));
		Zend_Registry::set('cache', $this->getBootstrap()->getResource('cachemanager')->getCache('general'));
		
		Kit_Model_Service::setDefaultDbAdapter($this->getBootstrap()->getResource('db'));
		Kit_Model_Service::setDefaultCache($this->getBootstrap()->getResource('cachemanager')->getCache('general'));
		Kit_Model_Service::setDefaultLog($this->getBootstrap()->getResource('log'));	
		Kit_Model_Service::setDefaultAuth(Zend_Auth::getInstance());       	
		
    }

}