<?php

class Kit_Application_Resource_Db extends Zend_Application_Resource_Db
{

    /**
     * Defined by Zend_Application_Resource_Resource
     *
     * @return Zend_Db_Adapter_Abstract|null
     */
    public function init ()
    {
        $db = parent::init();
        $this->getBootstrap()->bootstrap( 'cachemanager' );
        $cache = $this->getBootstrap()->getPluginResource( 'cachemanager' )->getCacheManager()->getCache('general');
        Zend_Db_Table::setDefaultMetaDataCache( $cache );
        
        Zend_Db_Table::setDefaultAdapter( $db );
        
        return $db;
    }
}