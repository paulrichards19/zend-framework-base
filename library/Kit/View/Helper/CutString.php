<?php

/**
 * CutString helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Kit_View_Helper_CutString extends Zend_View_Helper_Abstract
{

    /**
     * Cuts a string to the given length
     */
    public function cutString ( $string, $length, $pad = '...' )
    {
        $pad = ( strlen( $string ) > $length ) ? $pad : '';
        return ( substr( $string, 0, $length ) . $pad );
    }

}
