<?php

/**
 * TimeAgo helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Kit_View_Helper_TimeAgo extends Zend_View_Helper_Abstract
{

    /**
     * Returns a string representation of the time in the past twitter style
     */
    public function timeAgo ( $unixTime )
    {
    	if( empty($unixTime) ){
    		return 'never';
    	}
    	
        $now = time();
        $ago = $now - $unixTime;
        switch( $ago ){

        	case ( $ago < 60 ):
        	    // seconds
        	    if( $ago <= 1 ){
        	        $string = "1 sec ago";
        	    }else{
        	        $string = "$ago secs ago";
        	    }

        	break;

        	case ( $ago < (60*60) ):
        	    // mins
        	    $ago = floor( $ago /60 );
        	    if( $ago == 1 ){
        	    	$string = "1 min ago";
        	    }else{
        	    	$string = "$ago mins ago";
        	    }
        	break;

        	case ( $ago < (60*60*24) ):
        		// hours
        		$ago = floor( $ago /60 / 60 );
        		if( $ago == 1 ){
        			$string = "1 hour ago";
        		}else{
        			$string = "$ago hours ago";
        		}
        	break;

        	case ( $ago < (60*60*24*7) ):
        		// days
        		$ago = floor( $ago /60 / 60 / 24 );
        		if( $ago == 1 ){
        			$string = "1 day ago";
        		}else{
        			$string = "$ago days ago";
        		}
        	break;

        	case ( $ago < (60*60*24*7*8) ): // up to 8
        		// days
        		$ago = floor( $ago /60 / 60 / 24 / 7 );
        		if( $ago == 1 ){
        			$string = "1 week ago";
        		}else{
        			$string = "$ago weeks ago";
        		}
        	break;

        	case ( $ago < (60*60*24*7*52) ):
        		// days
        		$ago = floor( $ago /60 / 60 / 24 / 30 );
        		if( $ago == 1 ){
        			$string = "1 month ago";
        		}else{
        			$string = "$ago months ago";
        		}
        	break;

        	default:
        	    $string = 'over 1 year';
        	break;
        }

        return $string;


    }

}
