<?php

/**
 * Kit_View_Helper_GetPerformanceClass
 *
 * @uses viewHelper Zend_View_Helper
 */
class Kit_View_Helper_GetPerformanceClass extends Zend_View_Helper_Abstract
{

    /**
     * returns performance colour
     */

    public function getPerformanceClass($val, $posclass = 'positive', $negclass = 'negative', $neuclass = 'neutral') {

         if($val< 0)
         {
             return $negclass;
         }elseif ($val > 0) {
             return $posclass;
         } else {
             return $neuclass;
         }
    	
    }
	

}