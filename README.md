# Zend Framework Base App with Bootstrap

## Features
* Twitter bootstrap
* FirePHP DB profiling
* FirePHP Logging
* FirePHP Memory usage
* Zend Modules enabled
* DEV/LIVE ready application.ini
* File and Memcache Caching enabled